package app.com.study.stickyny.pizzaparty;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    TextView mResultText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText personEdit = (EditText) findViewById(R.id.person_edit);
        final EditText pizzaEdit = (EditText) findViewById(R.id.pizza_edit);
        final EditText sliceEdit = (EditText) findViewById(R.id.slice_edit);
        mResultText = (TextView) findViewById(R.id.result_text);

        TextWatcher textWatcher = new TextWatcher() {
            private String getStr(String type) {
                switch (type) {
                    case "person" :
                        return personEdit.getText().toString();
                    case "pizza" :
                        return pizzaEdit.getText().toString();
                    case "slice" :
                        return sliceEdit.getText().toString();
                }
                return "error";
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!getStr("person").isEmpty() && !getStr("pizza").isEmpty() && !getStr("slice").isEmpty()) {
                    if (isEvenNum(toInteger(getStr("slice")))) {
                        print(toInteger(getStr("person")),
                                toInteger(getStr("pizza")),
                                toInteger(getStr("slice"))
                        );
                    } else {
                        Toast.makeText(MainActivity.this, getString(R.string.error_msg_not_even), Toast.LENGTH_SHORT).show();
                        sliceEdit.setText("");
                        mResultText.setText("");
                    }
                }
            }
        };

        personEdit.addTextChangedListener(textWatcher);
        pizzaEdit.addTextChangedListener(textWatcher);
        sliceEdit.addTextChangedListener(textWatcher);

    }
    private int toInteger(String str) {
        return Integer.parseInt(str);
    }

    private boolean isEvenNum(int sliceNum) {
        if (sliceNum %2 == 0) {
            return true;
        } else {
            return false;
        }
    }

    private void print(int personNum, int pizzaNum, int sliceNum) {
        mResultText.setText(getString(
                R.string.result_text,
                eachPizzaCalc(personNum, pizzaNum, sliceNum),
                restPizzaCalc(personNum, pizzaNum, sliceNum))
        );
    }

    private int eachPizzaCalc(int personNum, int pizzaNum, int sliceNum) {
        return (pizzaNum * sliceNum) / personNum;
    }

    private int restPizzaCalc(int personNum, int pizzaNum, int sliceNum) {
        return (pizzaNum * sliceNum) % personNum;
    }
}
